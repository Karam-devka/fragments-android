package com.example.fragments.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.fragments.R;
import com.example.fragments.fragment.ContatosFragment;
import com.example.fragments.fragment.ConversasFragment;

public class MainActivity extends AppCompatActivity {

    Button botao_conversa;
    Button botao_contatos;
    ConversasFragment conversasFragment;
    ContatosFragment contatosFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        conversasFragment = new ConversasFragment();
        contatosFragment = new ContatosFragment();
        botao_conversa = findViewById(R.id.conversas_button);
        botao_contatos = findViewById(R.id.contatos_button);

        // COnfigurar objeto para fragmento
        botao_conversa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction(); // inicia transação
                transaction.replace(R.id.frame_conteudo,conversasFragment); // instancia o objeto que deve ser exibido
                transaction.commit(); // encerra transação
            }
        });
        botao_contatos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contatosFragment = new ContatosFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_conteudo,contatosFragment);
                transaction.commit();
            }
        });





    }

}